from django.db import models
from django.contrib import admin


class Dictionary(models.Model):
    word = models.CharField(max_length=100, primary_key=True)
    rus = models.CharField(max_length=100)

    def __str__(self):              
        return self.word