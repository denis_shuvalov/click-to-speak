
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from youtube_transcript_api import YouTubeTranscriptApi as yta
import requests
import json
from handler.models import Dictionary
import re

@csrf_exempt
def index(request):
	for a in request.POST.copy():
		print(a)
	url = request.POST.get('url')
	vid = url[url.rfind('=')+1:]
	dict_url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
	trans_options={
		'key':'trnsl.1.1.20190622T115104Z.714086443080e878.8f9beb54c5e29dd19039327d4e00553bb30533b6', 
		'lang':'en-ru', 
		'text':''
	}
	status = False
	try:
		subtitles = yta.get_transcript(vid, languages=['en'])
		words = {}
		status = True
	except(Exception):
		print('No subtitles')
		words = {}
		subtitles = [{'text':''}]
		status = False

	for sub in subtitles:
		for word in sub['text'].split():

			if word.find('...') != -1:
				word = word[:word.find('...')]
			if word[-1] == '\"':
				word = word[:-1]
			if len(word) == 0:
				continue
			if word[-1] in ['.',',',':',';','!','?',')',']','}']:
				word = word[:-1]
			if len(word) == 0:
				continue
			if word[-1] == '\"':
				word = word[:-1]
			if len(word) == 0:
				continue
			if word[0] in ['(','[','{','\"']:
				word = word[1:]
			if len(word) == 0:
				continue
			if not re.match(r'^\d+$',word):
				words[word] = 'тестовый'

	for word in words.keys():
		trans_options['text'] = word
		words_from_db = Dictionary.objects.filter(word=word)
		if len(words_from_db) == 0:
			web_ans = requests.get(dict_url,params=trans_options)
			json_ans = json.loads(web_ans.text)
			print(web_ans.text)
			words[word] = json_ans['text'][0]
			new_word = Dictionary(word=word, rus=json_ans['text'][0])
			new_word.save()
		else:
			print(word," -from db")
			words[word] = words_from_db[0].rus			
		
	return JsonResponse({'dictionary':words,'length':len(words),'status':status})
