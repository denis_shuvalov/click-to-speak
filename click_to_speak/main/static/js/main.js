
var app = new Vue({
  el: '#app',
  data: {
    headline: 'Please, enter a link to any Youtube video in English...',
    handler: 'http://127.0.0.1:8000/handler/',
    reg: 'Регистрация',
    log: 'Войти',
    words: {},
  	words_error: '',
  	wait: false,
  	activeBorder: '',
  	status: true,
  	loaded: false,
  	total: 0
  },
  methods: {
  	send: function(event){
  		this.loaded = false
  		this.status = true
  		this.words={}
  		this.wait = true
  		this.activeBorder = ''
  		let form_data = new FormData(event.target);
 		this.$http.post(this.handler, form_data).then(function (data) {
            // set data on vm
            this.words = data['body']['dictionary']
            this.wait = false
            this.status = data['body']['status']
            this.total = data['body']['length']
       
            if (this.status){
            	this.loaded = true
            	this.activeBorder = '1px solid #AAA'

            }
            
        }, function (err) {
            this.words_error = err['body']
        })
  	}
  },
  delimiters: ['[[',']]']
})